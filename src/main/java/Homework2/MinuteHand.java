package src.main.java.Homework2;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MinuteHand extends Clock{


    public MinuteHand(LocalDateTime time, LocalDate date) {
        super(time, date);
    }

    public int getMinuteHand(){
        return getTime().getMinute();
    }

    @Override
    public String toString() {
        return "Dziś jest: " + getDate() + ", wskazówka minutowa pokazuje: " + getTime().getMinute();
    }
}
