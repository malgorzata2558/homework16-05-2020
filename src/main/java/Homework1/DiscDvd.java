package src.main.java.Homework1;

public class DiscDvd {
   private String manufacturer;
   private int capacity;
   private boolean isAudio;

    public DiscDvd(String manufacturer, int capacity, boolean isAudio) {
        this.manufacturer = manufacturer;
        this.capacity = capacity;
        this.isAudio = isAudio;
    }

    public DiscDvd(int capacity) {
        this.capacity = capacity;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        if (isAudio == true) {
            return "Nazwa producenta to: " +
                    manufacturer + ", pojemność w gb to: "
                    + capacity + ", to płyta audio";
        } else return "Nazwa producenta to: " +
                 manufacturer + ", pojemność w gb to: "
                + capacity + ", to nie jest płyta audio";
    }

    public void play() {
        if (isAudio == true) {
            System.out.println("Playing");
        } else System.out.println("Disc is invalid");
    }
}
