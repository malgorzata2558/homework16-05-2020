package src.main.java.Homework1;

public class Main {
    public static void main(String[] args) {
        DiscDvd disc1 = new DiscDvd( "Verbatim", 50, true);
        DiscDvd disc2 = new DiscDvd("Titanum", 50, false);

        disc1.play();
        disc2.play();

        System.out.println(disc1.toString());
        System.out.println(disc2.toString());
    }
}
