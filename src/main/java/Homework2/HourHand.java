package src.main.java.Homework2;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class HourHand extends Clock {

    public HourHand(LocalDateTime time, LocalDate date) {
        super(time, date);
    }

    public int getHourHand() {
        return getTime().getHour();
    }

    @Override
    public String toString() {
        return "Dziś jest: " + getDate() + ", wskazówka godzinowa pokazuje: " + getTime().getHour();
    }

}
