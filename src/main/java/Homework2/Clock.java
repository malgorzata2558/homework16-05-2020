package src.main.java.Homework2;

import java.time.LocalDate;
import java.time.LocalDateTime;

//klasa reprezentuje zegar na kalendarzu windowsa, gdzie jest data i czas ;)
public class Clock {
    private LocalDateTime time;
    private LocalDate date;

    public LocalDateTime getTime() {
        return time;
    }


    public LocalDate getDate() {
        return date;
    }


    public Clock(LocalDateTime time, LocalDate date) {
        this.time = time;
        this.date = date;
    }

    public String toString() {
        return "Dziś jest: " + date + ", godzina: " + time;

    }
}
