package src.main.java.Homework3;

public class Company {
    private String name;
    private int manufacturingCost;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getManufacturingCost() {
        return manufacturingCost;
    }

    public void setManufacturingCost(int manufacturingCost) {
        this.manufacturingCost = manufacturingCost;
    }

    public Company(String name, int manufacturingCost) {
        this.name = name;
        this.manufacturingCost = manufacturingCost;
    }
}
