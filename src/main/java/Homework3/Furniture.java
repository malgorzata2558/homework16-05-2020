package src.main.java.Homework3;


public class Furniture {
    private String kindOfFurniture;
    private Company company;
    private Boards boards;

    public Furniture(String kindOfFurniture, Company company, Boards boards) {
        this.kindOfFurniture = kindOfFurniture;
        this.company = company;
        this.boards = boards;
    }

    public String getKindOfFurniture() {
        return kindOfFurniture;
    }

    public void setKindOfFurniture(String kindOfFurniture) {
        this.kindOfFurniture = kindOfFurniture;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Boards getBoards() {
        return boards;
    }

    public void setBoards(Boards boards) {
        this.boards = boards;
    }

    @Override
    public String toString() {
        return kindOfFurniture + "jest wyprodukowany przez firmę: " + company.getName() + " składa się z " + boards.getQuantity() + " desek" ;
    }

    public int costOfFurniture(){
        return company.getManufacturingCost() + (boards.getQuantity() * boards.getBoardsCost());
    }
}
