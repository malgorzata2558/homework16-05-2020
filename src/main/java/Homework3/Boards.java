package src.main.java.Homework3;

public class Boards  {
   private int boardsCost;
    private int quantity;

    public int getBoardsCost() {
        return boardsCost;
    }

    public void setBoardsCost(int boardsCost) {
        this.boardsCost = boardsCost;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Boards(int boardsCost, int quantity) {
        this.boardsCost = boardsCost;
        this.quantity = quantity;
    }
}
