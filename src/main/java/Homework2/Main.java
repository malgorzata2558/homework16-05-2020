package src.main.java.Homework2;


import java.time.LocalDate;
import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) {
        Clock clock1 = new Clock(LocalDateTime.now(), LocalDate.now());
        HourHand hourHand = new HourHand(LocalDateTime.now(), LocalDate.now());
        MinuteHand minuteHand = new MinuteHand(LocalDateTime.now(), LocalDate.now());

        System.out.println(clock1.toString());
        System.out.println(hourHand.toString());
        System.out.println(minuteHand.toString());

        Clock[] params = {clock1, hourHand, minuteHand};
        for (Clock clockParams  : params){
            System.out.println(clockParams);
        }
    }
}
